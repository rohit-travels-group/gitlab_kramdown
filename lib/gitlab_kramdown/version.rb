# frozen_string_literal: true

module GitlabKramdown
  VERSION = '0.10.0'
end
